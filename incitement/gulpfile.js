var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('custom.scss');
	mix.scripts('mainapp.js');
});
