<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XmlParser as XMLtoArray;

class XMLtoJSON extends Controller
{
    private $products = array();
    private $status   = 200;

    public function parseXML(Request $request)
    {
        $valid = validator()->make(
            $request->all(), [
            'url' => 'required'
        ]);
        if ($valid->fails()) {
            $this->status   = 400;
            $this->products = ['error' => 'XML URL not included'];
        } else {
            $XMLfile = $request->input('url');
            try {
                $xml            = XMLtoArray::load($XMLfile);
                $this->products = $xml->parse([
                    'products' => ['uses' => 'product[productID,name,price,price::currency>currency,categories.category(::path=@)>categories,productURL,imageURL]',
                        'default' => null]
                ]);
            } catch (\Exception $e) {
                $this->status   = 400;
                $this->products = ['error' => $e->getMessage()];
            }
        }

        return response()->json($this->products, $this->status);
    }
}