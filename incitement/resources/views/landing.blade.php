<!DOCTYPE html>
<html lang="en">
    <head>
        <title>XML to Grid</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type='text/css' href='https://fonts.googleapis.com/css?family=Pacifico'>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" type="text/css" href="/css/custom.css">
        <link rel="icon" href="/image/favicon.png">
    </head>
    <body ng-app="incitement" ng-controller="mainCtrl" ng-init="init()">
        <div class="loader ng-hide" ng-show="loader">
            <h5 ng-show="failure" class="fail white-text">Json Request Failed : <% errorMsg %></h5>
        </div>
        <header class="navbar-fixed">
            <nav>
                <div class="nav-wrapper center">
                    <a ng-class="{'rubberBand':productsList,'pulse':!productsList}" ng-click="productsList = false" href="#" class="brand-logo brand-logo-center animated">< XML beautifier ></a>
                </div>
            </nav>
        </header>
        <section class="container" ng-show="!productsList">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <div ng-class="{'flipInX':!productsList,'flipOutX':productsList}" class="card-panel valign-wrapper faded z-depth-4 margin-top-40 animated">
                        <div class="valign center-block">
                            <div class="row">
                                <div class="row">
                                    <div class="col m12">
                                        <div class="card-panel blue shadow-custom">
                                            <span class="white-text">
                                                Please include a link to a XML file in the field provided below. Once submitted the XML will be parsed and the results will be shown as a grid for each product included in the XML.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <form name="submitXML" ng-submit="submitForm()" novalidate>
                                    <div class="row">
                                        <div class="input-field col m12">
                                            <i class="material-icons prefix">link</i>
                                            <input name="xmlURL" ng-model="XML.url" id="icon_prefix" type="text" required>
                                            <label for="icon_prefix">Enter XML URL</label>
                                        </div>
                                    </div>
                                    <div class="center">
                                        <button ng-disabled="submitXML.$invalid" class="btn waves-effect waves-light blue darken-1" type="submit" name="action">Submit
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container" ng-show="productsList">
            <div class="row center">
                <h4 class="blue-text text-darken-2 cursive">Sort by</h4>
                <div class="col s4">
                    <div class="switch">
                        <p class="cursive flow-text blue-text text-darken-2 no-margin-top">price</p>
                        <label>
                            <span class="hide-on-small-only">Off</span>
                            <input ng-checked="!sort.sortChecked" ng-click="flipper()" type="checkbox">
                            <span class="lever"></span>
                            <span class="hide-on-small-only">On</span>
                        </label>
                    </div>

                </div>
                <div class="col s4">
                    <div class="switch">
                        <p class="cursive flow-text blue-text text-darken-2 no-margin-top">name</p>
                        <label>
                            <span class="hide-on-small-only">Off</span>
                            <input ng-checked="sort.sortChecked" ng-click="flipper()" type="checkbox">
                            <span class="lever"></span>
                            <span class="hide-on-small-only">On</span>
                        </label>
                    </div>
                </div>
                <div class="col s4">
                    <div class="switch">
                        <p class="cursive flow-text blue-text text-darken-2 no-margin-top">order</p>
                        <label>
                            <span class="hide-on-small-only">Ascending</span>
                            <input ng-click="sort.sortReverse = !sort.sortReverse" type="checkbox">
                            <span class="lever"></span>
                            <span class="hide-on-small-only">Descending</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row margin-top-40">
                <div ng-class="{'flipInY _<% $index %>':productsList}" class="col m4 s12 animated" ng-repeat="product in products | orderBy:sort.sortType:sort.sortReverse">
                    <div class="card z-depth-2">
                        <div class="card-image waves-effect waves-block">
                            <img alt="Product Image" class="activator responsive-img" src="<% product.imageURL %>">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4 truncate"><% product.name %><i class="material-icons right">more_vert</i></span>
                            <h6><b><% product.currency %></b> <i><% product.price %></i></h6>
                        </div>
                        <div class="card-action center">
                            <a class="btn-floating btn-large waves-effect waves-light blue darken-1" href="<% product.productURL %>" target="_blank"><i class="material-icons">open_in_new</i></a>
                        </div>

                        <div class="card-reveal">
                            <span class="card-title blue-text text-darken-3">Full Details<i class="material-icons right">close</i></span>
                            <div class="divider"></div>
                            <p><b>Product ID: </b> <% product.productID %></p>
                            <div class="divider"></div>
                            <p><b>Fullname: </b> <% product.name %></p>
                            <div class="divider"></div>
                            <p><b>Price: </b> <% product.price %></p>
                            <div class="divider"></div>
                            <p><b>Currency: </b> <% product.currency %></p>
                            <div class="divider"></div>
                            <p><b>Categories: </b></p>
                            <ul class="collection">
                                <li ng-repeat="category in product.categories" class="collection-item"><% category %></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
        <script src="/js/mainapp.js"></script>
    </body>
</html>