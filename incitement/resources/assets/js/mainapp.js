var myApp = angular.module('incitement', []);
myApp.config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%').endSymbol('%>');
    }]);

myApp.controller('mainCtrl', ['$scope', '$timeout', 'XMLservice', function ($scope, $timeout, XMLservice) {
        $scope.init = function () {
            $scope.productsList = false;
            $scope.loader = false;
            $scope.failure = false;
        };
        $scope.sort = {
            sortType: 'price',
            sortReverse: false,
            sortChecked: false
        };
        $scope.flipper = function () {
            ($scope.sort.sortType === 'price') ? $scope.sort.sortType = 'name' : $scope.sort.sortType = 'price';
            $scope.sort.sortChecked = !$scope.sort.sortChecked;
            console.log($scope.sort.sortType);
        };

        $scope.products = [];
        $scope.submitForm = function () {
            $scope.loader = true;
            XMLservice.getJson($scope.XML)
                    .then(function (response) {
                        $scope.loader = false;
                        $scope.products = response.data.products;
                        console.log(response.data.products);
                        $scope.productsList = true;
                    }, function (error) {
                        console.log(error);
                        $scope.loader = true;
                        $scope.failure = true;
                        $scope.errorMsg = error.data.error;
                        $timeout(function () {
                            $scope.loader = false;
                            $scope.failure = false;
                        }, 4000);
                    });
        };

    }]);

myApp.service('XMLservice', ['$http', 'apiConstant', function ($http, apiConstant) {
        var urlBase = apiConstant.server + apiConstant.api;
        this.getJson = function (data) {
            return $http.post(urlBase, data);
        };
    }]);

myApp.constant('apiConstant', {
    server: 'http://incitement.test', //CHANGE ME
    api: '/incitement/xml'
});