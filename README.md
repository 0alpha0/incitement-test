# Readme
First of all thank you for the opportunity to join the ever lovely incitement. In this document I will guide you through the steps to get the test up and running!
This documentation assumes you're running a windows machine.

### Step 1
Please make sure you have the following tools downloaded and setup on your machine:

    - Composer (https://getcomposer.org/download/)
    - Npm [Node.js] (https://nodejs.org/en/download/)
    - Xampp [Apache+PHP >= 5.6.4](https://www.apachefriends.org/download.html)
    
Once the packages above are install and running correctly you may proceed to setup the server on Xampp

### Step 2
After making sure everything is running and in order it's time to setup the environment in which we will run the SPA. 

Find the freshly installed Xampp and run it. Once in the dashboard you'll notice the modules installed along with start and stop buttons, but we're not going to start it yet! Before we start the __Apache__ module you must setup a virtual host.

The setup process is quite simple:
    
    1. Browse the folder where you installed Xampp and once in go to 
    apache/conf/extra
    2. Once in the folder you will notice many .conf configuration files
    find and open: httpd-vhosts.conf
    3. In the file paste the following configuartion at the end of the file:
    
```
<VirtualHost *:80>
    ServerAdmin your@email.com
    #point to the public folder of the project as shown below
    DocumentRoot "D:\test\incitement\public"
    #select a local domain name of your choice
    ServerName incitement.test
    #where the error logs should be named and kept in apache
    ErrorLog "logs/incitement.log"
    CustomLog "logs/incitement.log" common
    #directory permissions for smooth sailing
    <Directory "D:\test\incitement\public">
		Options All
		AllowOverride All
		Require all granted
    </Directory>
</VirtualHost>
```

Once the above steps are completed it's time to change Windows' own __hosts__ file. The hosts file can be found under __C:\Windows\System32\driver\etc__ . After opening the file with any editor (notepad works too) just add the foll0wing line to the end of the file
```
127.0.0.1	incitement.test
```

Now go back to Xampp's dashboard and start the __Apache__ server. If everything goes right you'll see the Apache badge light up green.
### Step 3
Now that you have your xampp local server setup and ready to pump apps, it's time to setup the project files. The initial setup is just a few commands but bear in mind it can take a while as it is dowloading all the dependencies of the project, which of course means an internet connection is required, a fast one preferably.

Open up a __command__ prompt from the start menu by typing __cmd__. Now browse on over to the project file and run the following command to download the dependencies:

```
composer install
```

Once the command is completed run the next command:

```
npm install
```

After the commands above are run and completed it's time to run the application.

### Step 4
Congratulations! You're now at the last step! All that's left is to point the angular file in the right direction to the API.

Inside the project folder (incitement) follow through to this path: __\resources\assets\js__. Once inside the you'll see the __mainapp.js__ file which has all of the AngularJS code within. Open the file with your favourite editor and scroll all the way down to the comment that says __"CHANGE ME"__ change the link to your local server link (currently http://incitement.test/) and you're done with the app setup!

Now to compile and finalize the application head back over to the projects main folder where the __package.json__ file is. Open up a command prompt and type in the following command:

```
npm run prod
```

This will compile and minify all of the project's resources and deploy them into their relevant folders in the public folder.

Your application is now ready to fire up!!

Hope the documentation was clear enough :) I'm open for any questions at 10alpha01@gmail.com. Hope you like it!